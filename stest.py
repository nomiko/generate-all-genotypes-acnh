from Flower import Flower


# map used to store remaining and owned genes for each flower
# mapping: <string, [str]>, key is flower name and value is 2 arrays, remaining genes and owned genes
stest_map = {
    "rose" : [
        ['0002', '0012', '0022', '0101', '0102', '0112', '0122', '0201', '0202', '0211', '0212', '0222', '1102', '1200', '1201', '1202', '1212', '2200', '2201', '2202'],
        ['0010', '0200', '2001', '0020', '0100', '0110', '1000', '1010', '1100', '1110', '2000', '2220', '0000', '1001', '1011', '1101', '1111', '1120', '1210', '1220', '2002', '2101', '2110', '2120', '2210', '0120', '0210', '0220', '1002', '1211', '1221', '2010', '2100', '2111', '2122', '2211', '2221', '0001', '0011', '0111', '1112', '1122', '2012', '2022', '2112', '2212', '0121', '0221', '1012', '1021', '1121', '1222', '2020', '2102', '2222', '0021', '1020', '2011', '2121', '1022', '2021']
    ],

    "tulip" : [
        ['002', '011', '012', '021', '022', '112', '121', '122', '212', '221', '222'],
        ['001', '020', '201', '100', '102', '111', '200', '000', '010', '101', '202', '110', '220', '120', '210', '211']
    ],

    "pansy" : [
        ['012', '021', '022', '112', '120', '121', '122', '210', '211', '212', '220', '221', '222'],
        ['001', '020', '200', '002', '011', '101', '110', '202', '000', '100', '111', '201', '010', '102']
    ],

    "cosmos" : [
        ['000', '002', '010', '020', '100', '102', '110', '120', '121', '202', '210', '220'],
        ['001', '021', '200', '012', '022', '101', '111', '011', '112', '122', '212', '221', '201', '211', '222']
    ],

    "lily" : [
        ['012', '021', '022', '112', '120', '121', '122', '210', '211', '212', '220', '221', '222'],
        ['002', '020', '201', '011', '100', '102', '111', '200', '000', '010', '101', '202', '001', '110']
    ],

    "hyacinth" : [
        ['012', '021', '022', '112', '121', '122', '212', '221', '222'],
        ['001', '020', '201', '002', '011', '100', '102', '111', '202', '000', '101', '210', '220', '010', '110', '120', '200', '211']
    ],

    "windflower" : [
        ['012', '021', '022', '112', '120', '121', '122', '210', '211', '212', '220', '221', '222'],
        ['001', '020', '200', '002', '011', '101', '110', '202', '000', '100', '111', '201', '010', '102']
    ],

    "mums" : [
        ['012', '021', '022', '102', '112', '121', '122', '201', '202', '211', '212', '221', '222'],
        ['001', '020', '200', '002', '011', '101', '110', '120', '210', '220', '000', '100', '111', '010']
    ]

}


def main():
    variant = input("Select a flower for stest: rose, tulip, pansy, cosmos, lily, hyacinth, windflower, mums: ")
    test_type = input("would you like to test using test version 1 or 2: ")

    if variant == "rose":
        line = input("the test could take upto 50+ hours for roses. Do you wish to proceed? y/n: ")
        if line.lower() != "y":
            return

    floo = Flower("stest")

    remaining = stest_map[variant][0]
    owned = stest_map[variant][1]

    remaining.sort()
    owned.sort()

    progress = input("Would you like to see the subset progress? y/n: ")
    if test_type == "1":
        genes_list = floo.missing_gene_test(variant, remaining, owned, progress)
    else:
        genes_list = floo.missing_gene_test2(variant, remaining, owned, progress)
    genes_list.sort()

    threshold = int(input("What is the maximum number of genotypes would you like to filter to: "))

    print("All possible subsets are: ")

    all_subsets = open("flowers.txt", "w")

    for subset in genes_list:
        line = ""
        for item in subset:
            line += item + " "
        line += "\n"
        all_subsets.write(line)

        if len(subset) <= threshold:
            print(subset)
    all_subsets.close()
    print("All results saved to Flower.txt!")


if __name__ == "__main__":
    main()
