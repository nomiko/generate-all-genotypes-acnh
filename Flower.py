import json     # used for json parsing and formatting
import random   # used for random assignments
rand = random.seed()

# Main class for handling parsing json data, thanks to Aeon
# and houses functions for color-safeness, determining if flower can be made and other various methods
class Flower:
    bwd = ""  # parsed json data for bwd
    fwd = ""  # parsed json data for fwd
    type = ""  # parsed json data for flowers

    colors = {}  # dictionary of all genotypes and respective colors
    flower_data = {}  # parsed json data for flowers[flower_type]
    seeds = []  # contains seeds for flower of self.type

    # holds all the genes that can be made from the key genes
    # mapping: <string, string>, key is every genotype in sorted order that can make the gene in value
    gene_map = {}

    # list of all valid flowers
    flower_list = ["rose", "tulip", "pansy", "cosmos", "lily", "hyacinth", "windflower", "mums"]

    testing = False

    # initialization function for Flower class
    #   type can either be one of the flowers or "test" for testing all flowers
    def __init__(self, type):
        self.type = type

        bwdf = open("flowers_bwd.json")
        fwdf = open("flowers_fwd.json")
        colorsf = open("flowers.json")

        bwdline = bwdf.read()
        fwdline = fwdf.read()
        colorline = colorsf.read()

        self.bwd = json.loads(bwdline)
        self.fwd = json.loads(fwdline)
        self.flower_data = json.loads(colorline)

        # if we are not testing, parse specific colors
        if type in self.flower_list:
            self.colors = self.flower_data[type]["breeds"]

        bwdf.close()
        fwdf.close()
        colorsf.close()

    # class method used for finding seed genotypes and the remaining genes one must find
    # prereq:   type must be defined
    # returns:  two arrays, array of remaining genes and array of seeds
    def find_remaining(self):
        remaining = []
        seeds = []
        sources = self.flower_data[self.type]["sources"]  # hold all data for seeds and island flowers

        # find all seeds and add to the list of seeds
        for flower in sources:
            if sources[flower] == "seeds":
                seeds.append(flower)

        self.seeds = seeds.copy()

        # remove all found seed genotypes from remaining genotypes
        for flower in self.colors:
            if flower not in seeds:
                remaining.append(flower)

        return remaining, seeds

    # class method used to determine color safeness given desired flower and parent 1 and parent 2
    # prereq:   type is well defined and self.colors is parsed
    # returns:  True if desired flower can be made color-safe, False otherwise
    def is_color_safe(self, gene1, gene2, desired):
        des_color = self.colors[desired]  # retrieve color of desired flower
        try:
            # if there exists more than one color from the two parents: gene1 and gene2, it will
            #   return a list of length greater than 1
            return len(self.fwd[self.type][gene1][gene2][des_color]) <= 1

        # exception is raised if gene1, gene2 or des_color does not exist in self.fwd mappings
        except KeyError:
            # simply return False as the flower cannot be made
            return False

    # class method used to determine if desired flower can be made from two parents: gene1 and gene2
    # prereq:   type is well defined and self.colors is parsed
    # returns:  True if desired flower can be made from gene1 and gene2
    def can_be_made(self, gene1, gene2, desired):
        des_color = self.colors[desired]  # retrieve color of desired flower
        try:
            # if the desired flower is indeed found in the mapping of type, gene1, gene2 and des_color, return True
            return desired in self.fwd[self.type][gene1][gene2][des_color]

        # exception is raised if gene1, gene2 or des_color does not exist in self.fwd mappings
        except KeyError:
            # return false since the desired flower does not exist
            return False

    # class method to run the test to find as much genotypes as it can
    # prereq:   self.type is well defined, flowers and owned passed as arguments are valid
    # returns:  list of remaining genotypes that could not be found and list of genotypes found
    def run_test(self, flowers, owned):
        counter = 0  # counter used to end the infinite loop once the test can no longer
        #   obtain any more genotypes with current list

        # loop until there are no more genes left to find
        while len(flowers) > 0:
            # for each remaining genotype
            for des in flowers:

                # holds all set of genotypes that can make the desired genotype
                # mapping: <string,[str]>, key is the chance that can make the desired flower and value is
                #          pair of genes that makes the desired genotype for chance key
                des_map = {}

                # holds all set of genotypes that can be used for testing the desired genotype
                # mapping: <string, <string, [str]>>, key is the chance that can make the desired flower,
                #           second key is the number of genotypes with same phenotype that be made from the genes from
                #           its second-key-value
                des_test_map = {}

                des_color = self.colors[des]    # color of current desired flower
                can_make = False                # used to indicate if desired genotype can be made color-safe
                found = False                   # used to indicate if desired genotype has been added

                # iterate through subsets of genes
                for gene1 in owned:
                    for gene2 in owned:
                        if self.can_be_made(gene1, gene2, des):

                            # if desired flower can be made from genes that are already obtained in color-safe way,
                            #   add the genes and its chance to look later
                            if self.is_color_safe(gene1, gene2, des):
                                # possibly further optimize this by including if it is highest chance and contains seed?
                                # may not be useful if we prioritize differently
                                chance = self.fwd[self.type][gene1][gene2][des_color][des]
                                if chance not in des_map:
                                    des_map[chance] = []
                                des_map[chance].append([gene1, gene2])
                                can_make = True

                            else:
                                # if we can already make the flower color-safe, don't bother testing
                                if can_make:
                                    continue

                                chance = self.fwd[self.type][gene1][gene2][des_color][des]
                                count = len(self.fwd[self.type][gene1][gene2][des_color])
                                if chance not in des_test_map:
                                    des_test_map[chance] = {}
                                if count not in des_test_map[chance]:
                                    des_test_map[chance][count] = []
                                des_test_map[chance][count].append([gene1, gene2])

                # evaluate which parents should be added
                #  for now, we'll prioritize highest chances and availability from seeds
                if can_make:
                    if len(des_map) > 0:
                        found = False
                    for chance in sorted(des_map, reverse=True):
                        for gene_sets in des_map[chance]:
                            if gene_sets[0] in self.seeds or gene_sets[1] in self.seeds:
                                if not self.testing:
                                    print(des + " can be made with " + gene_sets[0] + ", " + gene_sets[1] + "   " +
                                          str(self.fwd[self.type][gene_sets[0]][gene_sets[1]][des_color][des]/
                                           (2 ** (len(des)*2)) * 100
                                           ) + "%"
                                    )
                                owned.append(des)
                                found = True
                                counter = 0
                                break
                        if found:
                            flowers.remove(des)
                            break
                        else:
                            # if there were no flowers that can be made from seeds in highest chances,
                            #  add a random set from the highest chances
                            ran_num = random.randint(0,len(des_map[chance])-1)
                            gene1 = des_map[chance][ran_num][0]
                            gene2 = des_map[chance][ran_num][1]
                            if not self.testing:
                                print(des + " can be made with " + gene1 + ", " + gene2 + "   " +
                                    str(self.fwd[self.type][gene1][gene2][des_color][des] /
                                        (2 ** (len(des)*2)) * 100
                                        ) + "%"
                                )
                            owned.append(des)
                            flowers.remove(des)
                            counter = 0
                            break

                # if flower cannot be made in any color-safe way possible, try to test
                else:
                    if len(des_test_map) > 0:
                        found = False

                    # sort in descending order of chances
                    for chance in sorted(des_test_map, reverse=True):
                        # sort in ascending order of number of genotypes of same phenotype
                        for count in sorted(des_test_map[chance]):
                            for gene_sets in des_test_map[chance][count]:
                                #print("testing " + des + " with ", gene_sets)
                                tests, s_seed = self.test_gene(gene_sets[0], gene_sets[1], des)

                                # prioritize offsprings that can be made with highest chances and can be
                                #  verified by testing with least amount of genotypes from same phenotype
                                if len(tests) > 0:
                                    if not self.testing:
                                        print(
                                            des + " from " + gene_sets[0] + " and " + gene_sets[1] +
                                            " by " + str(
                                                self.fwd[self.type][gene_sets[0]][gene_sets[1]][des_color][des] /
                                                (2 ** (len(des)*2)) * 100
                                            ) + "%" +
                                            " can be tested with " + s_seed +
                                            ", verified by seeing " + str(tests))
                                    owned.append(des)
                                    flowers.remove(des)
                                    found = True
                                    counter = 0
                                    break
                            if found:
                                break
                        if found:
                            break

                if not found:
                    counter += 1
                    if counter % 100 == 0:
                        '''
                        if not self.testing:
                            print("Currently have: ")
                            print(flowers, owned, "\n")
                        '''
                        return flowers, owned

        return flowers, owned

    # class method used to test all flowers starting from seeds
    # pre-req:  type is not defined, it will be defined in this method
    # returns:  NULL, outputs all the possibly obtainable genes from each flowers starting from seeds
    def test(self):
        self.testing = True
        # holds all the result data
        # mapping: <string, [str]>, key is flower name and value is 2 arrays, unobtainable genes and obtained genes
        test_result = {}

        # iterate through flowers
        for flower in self.flower_list:
            # define colors and type here
            self.type = flower
            self.colors = self.flower_data[self.type]["breeds"]
            flowers, owned = self.find_remaining()

            print("starting for " + flower)
            flowers, owned = self.run_test(flowers, owned)
            test_result[flower] = [flowers, owned]

        # print test results
        for flower in test_result:
            print("for " + flower + ":")
            print("remaining: ", test_result[flower][0])
            print("owned: ", test_result[flower][1], "\n")

    # class method used to try and obtain the flower in a testing method for other phenotypes of the desired genotype
    # prereq:   self.type and self.seeds are well defined
    # returns:  list of genotypes that can be used to verify test and the seed, or empty list
    def test_gene(self, gene1, gene2, desired):

        des_color = self.colors[desired]

        # holds all the testing data
        # mapping: <string, <string, [string]>>, first key is gene, second key is seed, second-key-value is array of colors
        test_map = {}

        # list of genes that can be obtained from gene1 and gene2 that is same color as the desired genotype
        not_color_safe = self.fwd[self.type][gene1][gene2][des_color].copy()

        # loop for each gene and seed
        for gene in not_color_safe:
            test_map[gene] = {}
            for seed in self.seeds:
                # add all the colors obtainable from gene and seed
                colors = self.fwd[self.type][gene][seed].copy()
                color_list = []
                for color in colors:
                    color_list.append(color)
                test_map[gene][seed] = color_list

        # compare desired genotype with every other genotypes
        not_color_safe.pop(desired)

        # complete the comparison for each seed, and then each gene
        for seed in self.seeds:
            original_color = test_map[desired][seed]
            for gene in not_color_safe:
                colors = test_map[gene][seed]
                # for each color that cannot be used to verify, remove it from the desired genotype's color mapping
                for color in colors:
                    try:
                        original_color.remove(color)
                    except ValueError:
                        continue
            # if there is a color we can used to verify, return that with seed
            if len(original_color) > 0:
                return original_color, seed

        # we cannot verify the testing result, return empty set
        return [], ""

    # class method used to generate all subsets of the given genes
    # prereq:   n/a
    # returns:  all possible subsets of the genes list
    def generate_all_subsets(self, genes):
        subsets = []
        for gene in genes:
            i = len(subsets)
            for x in range(i):
                newset = subsets[x] + [gene]
                subsets.append(newset)
            subsets.append([gene])

        return subsets

    # class method used to test the remaining genes by making all subsets of the missing genes,
    #   then running the test to see if every gene can be found
    # prereq:   self.type and self.colors is well defined, use the pre-compiled list of remaining/owned or have it passed on
    # returns:  all subset of genes that can be added that completes the genes
    # param:    progress is used to note if user wants to see the code working since it takes quite a lot of time to run this
    def missing_gene_test(self, type, remaining, owned, progress):

        genes_list = []  # used to store all subsets that can complete the genotypes

        self.type = type

        self.colors = self.flower_data[self.type]["breeds"]

        # retrieve all subsets
        subsets = self.generate_all_subsets(remaining)
        subset_len = len(subsets)
        subset_count = 1

        # iterate for each subset
        for subset in subsets:
            if progress.lower() == "y":
                print("Currently working on subset " + str(subset_count) + "/" + str(subset_len) + ": ", subset)

            # burrow the original remaining and owned list
            test_remaining = remaining.copy()
            test_owned = owned.copy()

            # for each gene from the subset, add it to the owned list and remove from remaining
            for gene in subset:
                test_owned.append(gene)
                if gene in test_remaining:
                    test_remaining.remove(gene)

            # counter used to trigger done, done used to exit the while loop and indicate if all genotypes are found
            count = 0
            done = False

            subset_copy = subset.copy()

            # two exit conditions: len(test_remaining) <= 0 if all genotypes are found, or done = True if all
            #   genotypes cannot be found
            while len(test_remaining) > 0 and not done:
                for des in test_remaining:
                    found = False
                    for gene1 in subset_copy:
                        for gene2 in test_owned:
                            if self.can_be_made(gene1, gene2, des) and self.is_color_safe(gene1, gene2, des):
                                test_owned.append(des)
                                subset_copy.append(des)
                                found = True
                                count = 0
                                break
                        if found:
                            break
                    if found:
                        test_remaining.remove(des)
                    else:
                        count += 1
                        # if we cannot find all genotypes, exit the while loop
                        if count == 100:
                            done = True
                            break

            # if we exited the while loop in condition 1; found all genotypes, add the subset to genes_list
            if len(test_remaining) == 0 and not done:
                genes_list.append(subset)

            subset_count += 1
        # return all subset of genes that can be added to complete the genotypes
        return genes_list

    def missing_gene_test2(self, type, remaining, owned, progress):

        genes_list = []  # used to store all subsets that can complete the genotypes

        self.type = type

        self.colors = self.flower_data[self.type]["breeds"]

        # retrieve all subsets
        subsets = self.generate_all_subsets(remaining)
        subset_len = len(subsets)
        subset_count = 1

        # iterate for each subset
        for subset in subsets:
            if progress.lower() == "y":
                print("Currently working on subset " + str(subset_count) + "/" + str(subset_len) + ": ", subset)

            # burrow the original remaining and owned list
            test_remaining = remaining.copy()
            test_owned = owned.copy()

            # for each gene from the subset, add it to the owned list and remove from remaining
            for gene in subset:
                test_owned.append(gene)
                if gene in test_remaining:
                    test_remaining.remove(gene)

            # counter used to trigger done, done used to exit the while loop and indicate if all genotypes are found
            count = 0
            done = False

            subset_copy = subset.copy()

            gene = "0"
            while gene != "":
                gene = self.in_gene_map(test_owned)
                if gene != "":
                    test_owned.append(gene)
                    subset_copy.append(gene)
                    test_remaining.remove(gene)

            # two exit conditions: len(test_remaining) <= 0 if all genotypes are found, or done = True if all
            #   genotypes cannot be found
            while len(test_remaining) > 0 and not done:
                for des in test_remaining:
                    found = False
                    for gene1 in subset_copy:
                        for gene2 in test_owned:
                            if self.can_be_made(gene1, gene2, des) and self.is_color_safe(gene1, gene2, des):
                                test_owned.append(des)
                                subset_copy.append(des)
                                test_owned.sort()
                                line = ""
                                line.join(test_owned)
                                self.gene_map[line] = des
                                found = True
                                count = 0
                                break
                        if found:
                            break
                    if found:
                        test_remaining.remove(des)
                    else:
                        count += 1
                        # if we cannot find all genotypes, exit the while loop
                        if count == 100:
                            done = True
                            break

            # if we exited the while loop in condition 1; found all genotypes, add the subset to genes_list
            if len(test_remaining) == 0 and not done:
                genes_list.append(subset)

            subset_count += 1
        # return all subset of genes that can be added to complete the genotypes
        return genes_list

    def in_gene_map(self, owned):
        line = ""
        owned.sort()
        line = line.join(owned)
        if line in self.gene_map:
            return self.gene_map[line]
        else:
            return ""


def main():
    variant = input("Select a flower: rose, tulip, pansy, cosmos, lily, hyacinth, windflower, mums\nor test: ")

    floo = Flower(variant)  # initialization for class

    if variant == "test":
        floo.test()

    # specific test
    elif variant == "stest":
        progress = input("Would you like to see the subset progress? y/n: ")
        genes_list = floo.missing_gene_test(progress)
        genes_list.sort()

        threshold = int(input("What is the maximum number of genotypes would you like to see: "))

        print("All possible subsets are: ")

        all_subsets = open("flowers.txt", "w")

        for subset in genes_list:
            line = ""
            for item in subset:
                line += item + " "
            line += "\n"
            all_subsets.write(line)

            if len(subset) <= threshold:
                print(subset)
        # print("All possible subsets are: ", genes_list)
        all_subsets.close()
    # if we are not testing, find genotypes for specific flower type
    else:
        flowers = []
        owned = []

        flowers, owned = floo.find_remaining()

        # retrieve genotypes that user already has
        line = input("do you have any genes you'd like to add for " + variant + "? y/n: ")
        print("")
        if line.lower() == "y":
            while line.lower() != "end":
                line = input("enter the gene: ")
                # only add what we need to add
                if line in flowers:
                    flowers.remove(line)
                    owned.append(line)
                    print("added " + line + "\n")

        print("starting!...\n\n")

        flowers, owned = floo.run_test(flowers, owned)
        if len(flowers) > 0:
            print("Currently have: ")
            print(flowers, owned, "\n")


if __name__ == "__main__":
    main()
